const fs = require('fs');
const path = './messages';

const msgFiles = [];
const lastMessages = [];

const getFiles = () => {
  return new Promise((resolve, reject) => {
    fs.readdir(path, (err, files) => {
      if (err) {
        reject(err);
      } else {
        files.reverse().map((file, i) => {
          if (i <= 4) msgFiles.push(file);
        });
        resolve();
      }
    });
  });
};

const readFiles = file => {
  return new Promise((resolve, reject) => {
    fs.readFile(`${path}/${file}`, {encoding: 'utf-8'}, (err, result) => {
      if (err) {
        reject(err);
      } else {
        lastMessages.push(JSON.parse(result));
        resolve();
      }
    });
  });
};

const compare = (prev, next) => {
  if (prev.datetime > next.datetime) return -1;
  if (prev.datetime < next.datetime) return 1;
};

const db = {
  init: () => {
    return getFiles()
      .then(() => Promise.all(msgFiles.map(file => readFiles(file))))
      .catch(err => console.log(err));
  },

  getData: () => lastMessages.sort(compare),

  addMessage: msg => {
    msg.datetime = new Date();
    const message = JSON.stringify(msg);

    return new Promise((resolve, reject) => {
      fs.writeFile(`${path}/${msg.datetime}.msg`, message, err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
};

module.exports = db;