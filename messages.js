const express = require('express');
const router = express.Router();

const createRouter = db => {
  router.get('/', (req, res) => {
    res.send(db.getData());
  });

  router.post('/', (req, res) => {
    db.addMessage(req.body);
    res.send('Will create new message');
  });

  return router;
};

module.exports = createRouter;